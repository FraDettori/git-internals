package core;


import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.InflaterInputStream;

import static com.sun.xml.internal.ws.encoding.SOAPBindingCodec.UTF8_ENCODING;

public class GitBlobObject {
    String repoPath;
    String hash;
    Path ObjectPath;

    public GitBlobObject (String repoPath, String hash){
        this.repoPath = repoPath;
        this.hash = hash;
        ObjectPath = Paths.get(repoPath, "objects", hash.substring(0,2), hash.substring(2));
    }

    private String takeString() {
        try {
            FileInputStream fin = new FileInputStream(ObjectPath.toFile());
            InflaterInputStream iis = new InflaterInputStream(fin);
            byte[] bytes = new byte[1024];

            try {
                iis.read(bytes, 0 , 1024);
                return new String(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getType() {

        String fileHash = takeString();
        try {
            return takeType(fileHash);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }


    public String getContent() {
        try {
            FileInputStream fin = new FileInputStream(ObjectPath.toFile());
            InflaterInputStream iis = new InflaterInputStream(fin);
            byte[] bytes = new byte[1024];
            try {
                iis.read(bytes, 0 , 1024);
                return new String(bytes, Charset.forName(UTF8_ENCODING));


            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String takeType(String fileHash) throws IOException{
        return fileHash.substring(0, 4);
    }


}
