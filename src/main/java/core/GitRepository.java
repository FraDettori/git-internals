package core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.Buffer;

public class GitRepository {
    public String repoPath;


    public GitRepository (String repoPathInput){
            this.repoPath = repoPathInput;
    }

    public String getHeadRef() {
        try{
            BufferedReader br = new BufferedReader(new FileReader(repoPath+ "/HEAD"));
            String line = br.readLine();
            return line.substring(5);
        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    public String getRefHash(String s) {
        try{
            BufferedReader br = new BufferedReader(new FileReader(repoPath+"/refs/heads/master"));
            String hash = br.readLine();
            return hash;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
